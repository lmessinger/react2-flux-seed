import React from 'react';
import ActionCreator from '../actions/TodoActionCreators';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Input from 'react-bootstrap/lib/Input';
export default class CustomTextInput extends React.Component {
  constructor(props) {
    super(props);
  }

  handleToggle(task) {
    // (LM==> 13=) this will check the reference to the control (ref)
    //        and if it is, starts an update flow with the ActionCreator
    if (this.checkInput.getDOMNode().value==="on") {
      ActionCreator.completeTask(task);
    }
  }

getDefaultProps() {
    return {
      task: {
        title: '',
        completed: false
      }
    };
  }
   render() {
    
    let {task} = this.props;
    return (
      <ListGroupItem>
        {/* (LM==> 11=) we have to bind the handleToggle since on call time its this will not be this this. */}
        <input type="checkbox" ref={(input) => { this.checkInput = input; }} checked={task.completed}
                    onChange={this.handleToggle.bind(this, task)} label={task.title} />
        {task.title}
      </ListGroupItem>
    );
  }
}

var checkBox = React.createClass({
  checkInput:null,
  getDefaultProps() {
    return {
      task: {
        title: '',
        completed: false
      }
    };
  },

  handleToggle(task) {
    // (LM==> 13=) this will check the reference to the control (ref)
    //        and if it is, starts an update flow with the ActionCreator
    if (this.checkInput.getChecked()) {
      ActionCreator.completeTask(task);
    }
  },

  render() {
    var that = this;
    let {task} = this.props;
    return (
      <ListGroupItem>
        {/* (LM==> 11=) we have to bind the handleToggle since on call time its this will not be this this. */}
        <input type="checkbox" ref={(input) => { this.checkInput = input; }} checked={task.completed}
                    onChange={this.handleToggle.bind(this, task)} label={task.title} />
      </ListGroupItem>
    );
  }
});
checkBox.checkInput = null;

